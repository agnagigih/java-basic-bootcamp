public class MyFirstProgram {
    public static void main(String[] args) {
        System.out.println("Hello world!");
        System.out.println("Next Comment");
        myFavoriteFood();
        myFavoritePlace();
        myFavoriteFriend();
        myHobby();
    }
    // method
    // Favorite Food
    public static void myFavoriteFood(){
        System.out.println("My Favorite food:");
        System.out.println("1. Bakso");
        System.out.println("2. Nasi Goreng Tante (Tanpa Telur");
        System.out.println("3. Mei Ayam");
    }
    // Favorite place
    public static void myFavoritePlace(){
        System.out.println("My Favorite Place:");
        System.out.println("1. Home");
    }
    // Favorite Friend
    public static void myFavoriteFriend() {
        System.out.println("My Favorite Friend:");
        System.out.println("1. None");
    }
    // Favorite Hobby
    public static void myHobby() {
        System.out.println("My Hobby:");
        System.out.println("1. Nonton film/series");
    }
}