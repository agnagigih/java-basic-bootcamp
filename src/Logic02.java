public class Logic02 {
    public static void main(String[] args) {
        System.out.println("Soal 1");
        soal01(3);
        soal01(5);
        soal01(7);
        soal01(9);
        soal01(12);
        System.out.println("========================================================================================================");
        System.out.println("Soal 2");
        soal02(5);
        soal02(7);
        soal02(9);
        soal02(11);
        soal02(14);
        System.out.println("========================================================================================================");
        System.out.println("Soal 3");
        soal03(7);
        soal03(8);
        soal03(9);
        soal03(10);
        soal03(11);
        System.out.println("========================================================================================================");
        System.out.println("Soal 4");
        soal04(6);
        soal04(7);
        soal04(8);
        soal04(9);
        soal04(10);
        System.out.println("========================================================================================================");
        System.out.println("Soal 5");
        soal05(7);
        soal05(9);
        soal05(10);
        soal05(11);
        soal05(13);
        System.out.println("========================================================================================================");
        System.out.println("Soal 6");
        soal06(7);
        soal06(9);
        soal06(10);
        soal06(11);
        soal06(13);
        System.out.println("========================================================================================================");

    }

    public static void soal01(int n) {
        String[][] array01 = new String[n][n];
        for (int baris01 = 0; baris01<n; baris01++){
            int number = 0;
            for (int kolom01 = 0; kolom01<n; kolom01++ ){
                number++;
                if (baris01 == kolom01 || baris01 == n-1-kolom01){
                    array01[baris01][kolom01] = String.valueOf(number);
                } else array01[baris01][kolom01] = "";
                System.out.printf(array01[baris01][kolom01] + "\t");
            }
            System.out.println();
        }
        System.out.println("--------------------------------------------------------------------------------------------------------");
    }
    public static void soal02(int n){
        String[][] array02 = new String[n][n];
        for (int baris02 = 0; baris02<n; baris02++){
            int number02 = 1;
            for (int kolom02=0; kolom02<n; kolom02++){
                number02 += 2;
                array02[baris02][kolom02] = (baris02==0 || baris02 == n-1 || kolom02 == 0 || kolom02 == n-1) ? String.valueOf(number02): "";
                System.out.print(array02[baris02][kolom02] + "\t");
            }
            System.out.println();
        }
        System.out.println("--------------------------------------------------------------------------------------------------------");
    }
    public static void soal03(int n){
        String[][] array03 = new String[n][n];
        for (int baris03=0; baris03<n; baris03++){
            for (int kolom03=0; kolom03<n; kolom03++){
                if (baris03 == kolom03 || baris03 == n-1-kolom03 || baris03==0 || baris03 == n-1 || kolom03 == 0 || kolom03 == n-1) {
                    array03[baris03][kolom03] = String.valueOf(kolom03*2);
                } else array03[baris03][kolom03] = "";
                System.out.print(array03[baris03][kolom03]+"\t");
            }
            System.out.println();
        }
        System.out.println("--------------------------------------------------------------------------------------------------------");
    }
    public static void soal04(int n){
        int[] array04 = new int[n];
        String[][] array04Str = new String[n][n];
        for (int baris04=0; baris04<n; baris04++){
            for (int kolom04=0; kolom04<n; kolom04++){
                array04[kolom04] = (kolom04 <= 1) ? 1 : array04[kolom04-1] + array04[kolom04-2];
                if (n%2==0){
                    if (baris04==0 || baris04 == (n/2) || baris04 == (n/2) - 1 || baris04 == n-1 || kolom04==0 || kolom04 == (n/2) || kolom04 == (n/2) - 1 || kolom04 == n-1){
                        array04Str[baris04][kolom04] = String.valueOf(array04[kolom04]);
                    } else array04Str[baris04][kolom04] = "";
                    System.out.print(array04Str[baris04][kolom04]+"\t");
                } else{
                    if (baris04==0 || baris04 == (n/2) || baris04 == n-1 || kolom04==0 || kolom04 == (n/2) || kolom04 == n-1){
                        array04Str[baris04][kolom04] = String.valueOf(array04[kolom04]);
                    } else array04Str[baris04][kolom04] = "";
                    System.out.print(array04Str[baris04][kolom04]+"\t");
                }
            }
            System.out.println();
        }
        System.out.println("--------------------------------------------------------------------------------------------------------");

    }
    public static void soal05(int n){
        int[] array05 = new int[n];
        String[][] array05Str = new String[n][n];
        for (int baris05=0; baris05<n; baris05++){
            for (int kolom05=0; kolom05<n; kolom05++){
                array05[kolom05] = (kolom05 <= 2) ? 1 : array05[kolom05 - 1] + array05[kolom05 - 2] + array05[kolom05 - 3];
                if (kolom05 <= baris05 && kolom05 <= (n - baris05 - 1) || kolom05 >= baris05 && kolom05 >= (n - baris05 - 1) ){
                    array05Str[baris05][kolom05] = String.valueOf(array05[kolom05]);
                } else array05Str[baris05][kolom05] = "";

                System.out.print(array05Str[baris05][kolom05]+"\t");
            }
            System.out.println();
        }
        System.out.println("--------------------------------------------------------------------------------------------------------");
    }

    public static void soal06(int n){
        int[] array06 = new int[n];
        String[][] array06Str = new String[n][n];
        for (int baris06=0; baris06<n; baris06++){
            for (int kolom06=0; kolom06<n; kolom06++){
                array06[baris06] = (baris06 <= 1) ? 1 : array06[baris06-1] + array06[baris06-2];
                if (kolom06 <= baris06 && kolom06 >= (n - baris06 - 1) || kolom06 >= baris06 && kolom06 <= (n - baris06 - 1) ){
                    array06Str[baris06][kolom06] = String.valueOf(array06[baris06]);
                } else array06Str[baris06][kolom06] = "";

                System.out.print(array06Str[baris06][kolom06]+"\t");
            }
            System.out.println();
        }
        System.out.println("--------------------------------------------------------------------------------------------------------");
    }

}
