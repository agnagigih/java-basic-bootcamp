public class Logic01 {
    public static void main(String[] args) {
        System.out.println("Soal 1");
        soal01(4);
        soal01(6);
        soal01(8);
        soal01(10);
        soal01(12);
        System.out.println("-------------------------------------------------------------------------------------------");
        System.out.println("Soal 2");
        soal02(5);
        soal02(7);
        soal02(9);
        soal02(11);
        soal02(13);
        System.out.println("-------------------------------------------------------------------------------------------");
        System.out.println("Soal 3");
        soal03(6);
        soal03(7);
        soal03(8);
        soal03(9);
        soal03(10);
        System.out.println("-------------------------------------------------------------------------------------------");
        System.out.println("Soal 4");
        soal04(5);
        soal04(7);
        soal04(9);
        soal04(11);
        soal04(13);
        System.out.println("-------------------------------------------------------------------------------------------");
        System.out.println("Soal 5");
        soal05(6);
        soal05(7);
        soal05(8);
        soal05(9);
        soal05(10);
        System.out.println("-------------------------------------------------------------------------------------------");
        System.out.println("Soal 6");
        soal06(8);
        soal06(9);
        soal06(11);
        soal06(13);
        soal06(16);
        System.out.println("-------------------------------------------------------------------------------------------");
        System.out.println("Soal 7");
        soal07(24);
        soal07(25);
        soal07(26);
        soal07(27);
        soal07(28);
        System.out.println("-------------------------------------------------------------------------------------------");
        System.out.println("Soal 8");
        soal08(20);
        soal08(22);
        soal08(24);
        soal08(26);
        soal08(28);
        System.out.println("-------------------------------------------------------------------------------------------");
        System.out.println("Soul 9");
        soal09(7);
        soal09(8);
        soal09(9);
        soal09(10);
        soal09(11);
        System.out.println("-------------------------------------------------------------------------------------------");
        System.out.println("Soal 10");
        soal10(8);
        soal10(9);
        soal10(10);
        soal10(11);
        soal10(12);
        System.out.println("-------------------------------------------------------------------------------------------");

    }

    public static void soal01(int n){
        int[] deret01 = new int[n];
        for (byte index01=0; index01 < n; index01++){
            deret01[index01] = index01+1;
            System.out.print(deret01[index01]+"\t");
        }
        System.out.println();
    }
    public static void soal02(int n){
        int[] deret02 = new int[n];
        int odd = 0;
        int even = 0;
        for (int index02=0; index02 < n; index02++){
            if (index02 % 2 == 0){
                even++;
                deret02[index02] = even;
            } else{
                odd+=3;
                deret02[index02] = odd;;
            }
            System.out.print(deret02[index02]+"\t");
        }
        System.out.println();
    }
    public static void soal03(int n){
        int[] deret03 = new int[n];
        for (int index03=0; index03<n; index03++){
            deret03[index03] = index03*2;
            System.out.print(deret03[index03]+"\t");
        }
        System.out.println();
    }
    public static void soal04(int n) {
        int[] deret04 = new int[n];
        for (int index04 = 0; index04 < deret04.length; index04++) {
            deret04[index04] = (index04 <= 1) ? 1 : deret04[index04 - 1] + deret04[index04 - 2];
            System.out.print(deret04[index04]+"\t");
        }
        System.out.println();
    }
    public static void soal05(int n) {
        int[] deret05 = new int[n];
        for (int index05 = 0; index05 < deret05.length; index05++) {
            deret05[index05] = (index05 <= 2) ? 1 : deret05[index05 - 1] + deret05[index05 - 2] + deret05[index05 - 3];
            System.out.print(deret05[index05]+"\t");
        }
        System.out.println();
    }
    public static void soal06(int n) {
        int[] deret06 = new int[n];
        int addMod3_0 = 0;
        int addMod3_2 = 8;
        for (int index06=0; index06<n; index06++){
            if (index06 <=2) {
                deret06[0] = 2;
                deret06[1] = 3;
                deret06[2] = 5;
            } else {
                if (index06 % 3 == 0) {
                    addMod3_0+=5;
                    deret06[index06] = deret06[index06 - 3] + addMod3_0;
                } else if (index06 % 3 == 1) {
                    deret06[index06] = deret06[index06 - 3] + 8;
                } else{
                    deret06[index06] = deret06[index06 - 3] + addMod3_2;
                    addMod3_2+=2;
                }
            }
            System.out.print(deret06[index06]+"\t");
        }
        System.out.println();
    }
    public static String[] arrayAlphabet(){
        var alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        String[] strArray = null;
        strArray = alphabet.split("");
        return strArray;
    }
    public static void soal07(int n) {
        String[] deret07 = new String[n];
        for (int index07 = 0; index07 < n; index07++){
            if (index07 < 26){
                deret07[index07] = arrayAlphabet()[index07];
                System.out.print(deret07[index07]+"\t");
            } else {
                deret07[index07] = arrayAlphabet()[index07 - 26];
                System.out.print(deret07[index07] + "\t");
            }
        }
        System.out.println();
    }
    public static void soal08(int n) {
        String[] deret08 = new String[n];
        int oddIndex = 0;
        for (int index08=0; index08<n; index08++){
            if (index08 % 2 == 0){
                deret08[index08] = (index08<26) ? arrayAlphabet()[index08] : arrayAlphabet()[index08-26];
            } else {
                oddIndex+=2;
                deret08[index08] = String.valueOf(oddIndex);
            }
            System.out.print(deret08[index08]+"\t");
        }
        System.out.println();
    }
    public static void soal09(int n){
        int[] deret09 = new int[n];
        for (int index09 = 0; index09<n; index09++ ) {
            deret09[index09] = (int)Math.pow(3, index09);
            System.out.print(deret09[index09]+"\t\t");
        }
        System.out.println();
    }
    public static void soal10(int n){
        int[] deret10 = new int[n];
        for (int index10=0; index10<n; index10++){
            deret10[index10] = (int)Math.pow(index10, 3);
            System.out.print(deret10[index10]+"\t\t");
        }
        System.out.println();
    }
}
